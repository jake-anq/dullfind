#include <string>
#include <iostream>
#include <limits>
#include <cmath>
#include <algorithm>

#include <opencv2/opencv.hpp>

using namespace cv;

/** Convert HSV value back to RGB.
 *
 * Probably there's an OpenCV way to do this, but I don't feel like changing the code...
*/
Scalar HSVtoRGB(float fH, float fS, float fV) {
	float fR, fG, fB;
	
	float fC = fV * fS; // Chroma
	float fHPrime = fmod(fH / 60.0, 6);
	float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
	float fM = fV - fC;

	if(0 <= fHPrime && fHPrime < 1) {
		fR = fC;
		fG = fX;
		fB = 0;
	} else if(1 <= fHPrime && fHPrime < 2) {
		fR = fX;
		fG = fC;
		fB = 0;
	} else if(2 <= fHPrime && fHPrime < 3) {
		fR = 0;
		fG = fC;
		fB = fX;
	} else if(3 <= fHPrime && fHPrime < 4) {
		fR = 0;
		fG = fX;
		fB = fC;
	} else if(4 <= fHPrime && fHPrime < 5) {
		fR = fX;
		fG = 0;
		fB = fC;
	} else if(5 <= fHPrime && fHPrime < 6) {
		fR = fC;
		fG = 0;
		fB = fX;
	} else {
		fR = 0;
		fG = 0;
		fB = 0;
	}

	fR += fM;
	fG += fM;
	fB += fM;
	
	return Scalar(fR,fG,fB);
}

int main(int argc, char ** argv) {
	// The brute-force way to argparse!
	if(argc != 4) {
		std::cerr << "Find the most boring subrectangle with the given dimensions in an image\nUsage: " << argv[0] << " FILE x y\n";
		return -1;
	}
	
	// Read the image
	Mat image, src_gray;
	image = imread(argv[1], 1);
	
	if(!image.data) {
		std::cerr << "Couldn't load image :(\n";
		return -1;
	}
	
	// Blur it a bit, because we can I guess (I don't remember why - probably the edge detection works better)
	GaussianBlur(image, image, Size(3,3), 0, 0, BORDER_DEFAULT);
	// Convert to grayscale - we ignore colour information for dull-area detection
	cvtColor(image, src_gray, COLOR_BGR2GRAY);
	
	
	Mat grad_x, grad_y, abs_grad_x, abs_grad_y, grad;
	
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;
	// Highlight edges, since we consider these "interesting"
	// Gradient X
	Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
	// Gradient Y
	Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
	
	convertScaleAbs( grad_x, abs_grad_x );
	convertScaleAbs( grad_y, abs_grad_y );
	addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

	Mat grad_int;
	// Calculate the integral so we can try many different sub-regions quickly
	integral(grad, grad_int);
	
	size_t target_x = atoi(argv[2])+1;
	size_t target_y = atoi(argv[3])+1;
	
	int best_x = 0, best_y = 0, bestsum = std::numeric_limits<int>::max();
	
	// Brute-force search of all possible areas.
	for(size_t y=0; y<grad_int.rows - target_y; ++y) {
		int * row = grad_int.ptr<int>(y);
		int * row2 = grad_int.ptr<int>(y + target_y);
		
		for(size_t x=0; x<image.cols - target_x; ++x) {
			int sum = row2[x+target_x] - row[x+target_x] - row2[x] + row[x];
			//std::cout << sum << "\n";
			if(sum < bestsum) {
				bestsum = sum;
				best_x = x;
				best_y = y;
			}
		}
	}
	Rect roi(best_x,best_y,target_x,target_y);
	
	//namedWindow("Display Image", WINDOW_AUTOSIZE );
	//rectangle(grad, roi, Scalar(128,0,255));
	//imshow("Display Image", grad);
	//waitKey(0);
	
	Mat image_roi = image(roi);
	
	Scalar avg = mean(image_roi);
	
	float R=pow(avg.val[0]/255.0f,2.2); //hue
	float G=pow(avg.val[1]/255.0f,2.2); //saturation
	float B=pow(avg.val[2]/255.0f,2.2); //value
	
	float Y = 0.2126*R + 0.7152*G + 0.0722*B;
	float S = (std::max(std::max(R, G), B) - std::min(std::min(R, G), B)) / std::max(std::max(R, G), B);
	
	Scalar target;
	if(S < 0.4) {
		if(Y > 0.5)
			target = Scalar(0,0,0); // Light image - use black
		else
			target = Scalar(1,1,1); // Dark image - use light
	} else {
		// Pick a colour with opposite hue to the average and full saturation/value
		float H = atan2(sqrt(3.0)*(G-B)/2.0,0.5*(2.0*R - G - B));
		H += M_PI;
		if(H > M_2_PI)
			H -= M_2_PI;
		target = HSVtoRGB(H*180.0/M_2_PI,1.0,1.0);
	}
	
	std::cout << "Coordinates: " << (best_x + (target_x-1)/2) << ',' << (best_y + (target_y-1)/2) << '\n';
	
	std::cout << "Average: " << avg.val[0] << ',' << avg.val[1] << ',' << avg.val[2] << '\n';
	std::cout << "Contrast: " << (target.val[0]*255.0) << ',' << (target.val[1]*255.0) << ',' << (target.val[2]*255.0) << '\n';
	
	std::cout << "Bestsum: " << bestsum << "\n";
	
	return 0;
}			
