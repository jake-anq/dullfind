# Dullfind

A simple tool to find the least-interesting area of an image and a suitable contrasting colour to use to display something in that area.
